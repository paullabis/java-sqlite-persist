package com.sample;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Calendar;
import java.util.List;
import java.util.function.Function;
import java.util.stream.*;

public class Main {

    private static final String FILE_LOCATION = "./reputation.data.txt";
    private static final int MAX_BATCH_RECORD = 10000;
    private final static Function<String, Record> stringToRecord = Record::read;

    public static void main(String[] args) throws Exception {

        long before = Calendar.getInstance().getTimeInMillis();

        Class.forName("org.sqlite.JDBC");
        Connection connection = null;

        int totalLines;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:sample.db");
            connection.setAutoCommit(false);

            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);

            statement.executeUpdate(prepareDatabaseTable());


            List<String> lines = Files.lines(Paths.get(FILE_LOCATION)).collect(Collectors.toList());
            totalLines = lines.size();
            int count = 0;
            for (int index = 0; index < totalLines; index++) {

                printProgBar(((index + 1) * 100) / totalLines);

                statement.addBatch(stringToRecord.apply(lines.get(index)).createStatement());
                if (++count >= MAX_BATCH_RECORD || index == (totalLines - 1)) {
                    statement.executeBatch();
                    connection.commit();
                    count = 0;
                }
            }
        } finally {
            try {
                if (connection != null) {
                    connection.setAutoCommit(true);
                    connection.close();
                }
            } catch (SQLException sqle) {
                System.err.println(sqle);
            }
        }

        long after = Calendar.getInstance().getTimeInMillis();
        System.out.println(String.format("\nReading from file and persisting to SQLite database a total of %s records cost %s seconds.", totalLines, (after - before) / 1000.0));
    }

    public static void printProgBar(int percent){
        StringBuilder bar = new StringBuilder("[");

        for(int i = 0; i < 50; i++){
            if( i < (percent/2)){
                bar.append("=");
            }else if( i == (percent/2)){
                bar.append(">");
            }else{
                bar.append(" ");
            }
        }

        bar.append("]   ").append(percent).append("%     ");
        System.out.print("\r" + bar.toString());
    }

    private static String prepareDatabaseTable() {
        return "CREATE TABLE if NOT EXISTS visitor_record (" +
                "id integer not null primary key autoincrement" +
                ", hostIp varchar(255)" +
                ", dataReliability int" +
                ", riskFactor int" +
                ", activity varchar(255)" +
                ", country varchar(255)" +
                ", city varchar(255)" +
                ", latitude double" +
                ", longitude double" +
                ", miscellaneous varchar(255))"
                ;
    }
}
