package com.sample;

/**
 * Created on 5/24/15.
 *
 * @author pLabis
 */
public class Record {

    private String hostIp;
    private int dataReliability;
    private int riskFactor;
    private String activity;
    private String country;
    private String city;
    private double latitude;
    private double longitude;
    private String miscellaneous;

    public Record(String hostIp, int dataReliability, int riskFactor, String activity, String country, String city, double latitude, double longitude, String miscellaneous) {
        this.hostIp = hostIp;
        this.dataReliability = dataReliability;
        this.riskFactor = riskFactor;
        this.activity = activity;
        this.country = country;
        this.city = city;
        this.latitude = latitude;
        this.longitude = longitude;
        this.miscellaneous = miscellaneous;
    }

    public static Record read(String line) {
        if (line == null || line.length() == 0) return null;

        String[] data = line.split("#");
        String[] latLong = data[6].split(",");

        return new Record(
                data[0],
                Integer.valueOf(data[1]),
                Integer.valueOf(data[2]),
                data[3],
                data[4],
                data[5],
                Double.parseDouble(latLong[0]),
                Double.parseDouble(latLong[1]),
                data[7]
        );

    }

    public String createStatement() {
        return "INSERT INTO visitor_record (" +
                "hostIp " +
                ", dataReliability " +
                ", riskFactor " +
                ", activity " +
                ", country " +
                ", city " +
                ", latitude " +
                ", longitude " +
                ", miscellaneous "
                + ") VALUES" + toQueryParam();
    }

    @Override
    public String toString() {
        return "Record{" +
                "hostIp='" + hostIp + '\'' +
                ", dataReliability=" + dataReliability +
                ", riskFactor=" + riskFactor +
                ", activity='" + activity + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", miscellaneous='" + miscellaneous + '\'' +
                '}';
    }

    public String toQueryParam() {
        return String.format("(\"%s\",%s,%s,\"%s\",\"%s\",\"%s\",%s,%s,\"%s\")"
                ,hostIp, dataReliability, riskFactor, activity, country, city, latitude, longitude, miscellaneous);
    }


}
